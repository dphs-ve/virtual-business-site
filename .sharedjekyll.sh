#!/usr/bin/env bash
for location in $HOME/.rvm /usr/local/rvm; do
  [ -e "$location" ] && source "$location"/scripts/rvm
done
rvm use default
function jekyllcmd () {
    bundle exec jekyll "$@"
}