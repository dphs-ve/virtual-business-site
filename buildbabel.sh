#!/bin/bash
source .sharedbabel.sh
len=${#inputbabel[@]}
for i in $(seq 0 $((len-1))); do
    echo babel "${inputbabel[$i]}" --out-dir "${outputbabel[$i]}"
    babel "${inputbabel[$i]}" --out-dir "${outputbabel[$i]}"
done
