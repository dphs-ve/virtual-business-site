requirejs(["jquery", "babel/ui"], function defineSearchBarModule($, ui) {
    $("#searchbar-form").on("submit", function submitSearch(e) {
        e.preventDefault();
        var text = $("#searchbar-query").val();
        ui.pushAlert("You searched for " + text);
    });
    $("#searchbar").children(".input-group").focus(function(e){
        $(this).parent().addClass("input-group-highlight");
    }).blur(function(e){
        $(this).parent().removeClass("input-group-highlight");
    });
    return {};
});