define(["react", "reactbs", "babel/ui", "internal/firebase", "noop", "jquery"],
    function definePlaceholderModule(React, ReactBS, ui, fb, noop, $) {
        var exports = {};
        var fire = fb.thisFB;
        var BUY = 0;
        var INFO = 1;
        var STORE = 2;
        var RETURN = 3;
        exports.Button = React.createClass({
            getType: function getButtonType() {
                return this.props.type;
            },
            getStyle: function getButtonBootstrapStyle() {
                var type = this.getType();
                if (type === BUY) {
                    return "success";
                } else if (type === RETURN) {
                    return "primary";
                } else {
                    return "info";
                }
            },
            getText: function getButtonText() {
                var type = this.getType();
                if (type === BUY) {
                    return "Buy me!";
                } else if (type === INFO) {
                    return "Info";
                } else if (type === STORE) {
                    return "Return to the store";
                } else if (type === RETURN) {
                    return "Return to checkout";
                }
            },
            handleClick: function buttonClicked() {
                var type = this.getType();
                if (type === BUY) {
                    window.location = this.props.itemUrl;
                } else if (type === INFO) {
                    this.props.showFunc();
                } else if (type === STORE) {
                    this.props.showStore();
                } else if (type === RETURN) {
                    // The VEI cart URL.
                    window.location = "https://portal.veinternational.org/buybuttons/us021333/cart/";
                }
            },
            render: function renderButton() {
                return <ReactBS.Button bsStyle={this.getStyle()}
                                       onClick={this.handleClick}>
                    {this.getText()}
                </ReactBS.Button>;
            }
        });
        exports.Product = React.createClass({
            showProduct: function showThisProduct() {
                window.location.hash = "#" + this.props.item.itemId + "@store";
            },
            showStore: function showStore() {
                window.location.hash = "";
            },
            renderSingular: function renderProductNoTable(name, price, imageUrl, itemUrl, isVei, desc) {
                return <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-4">
                            <h4>
                                { name.name } - ${ price }
                            </h4>
                            { desc }
                        </div>
                        <div className="col-md-4">
                            <img className="really-nice-image img-responsive" src={ imageUrl }/>
                        </div>
                        { isVei ?
                            ""
                            :
                            <div className="col-md-2" style={{textAlign: "center"}}>
                                <exports.Button showStore={this.showStore} type={STORE}/>
                            </div>
                        }
                        <div className={"col-md-2" + (isVei ? " col-md-offset-1" : "")}
                             style={{textAlign: "center"}}>
                            <exports.Button itemName={ name } itemPrice={ price } itemUrl={ itemUrl }
                                            buyButton={isVei} vei={isVei} type={isVei ? RETURN : BUY}/>
                        </div>
                    </div>
                </div>;
            },
            renderList: function renderProductForTable(name, price, imageUrl, itemUrl, isVei, desc) {
                return <tr style={{padding: "10px, 0"}}>
                    <td style={{textAlign: "center", backgroundColor: "#eee", borderRadius: 5}}>
                        <img src={ imageUrl }
                             style={{width: "auto", height: 64}}/>
                    </td>
                    <td style={{width: "100%"}}>
                        <h4>
                            { name.name } - ${ price }
                        </h4>
                        { desc }
                    </td>
                    <td>
                        <exports.Button showFunc={this.showProduct} type={INFO}/>
                    </td>
                    <td>
                        <exports.Button itemName={ name } itemPrice={ price } itemUrl={ itemUrl }
                                        buyButton={true} type={BUY}/>
                    </td>
                </tr>;
            },
            render: function renderProduct() {
                var name = this.props.item.name;
                var price = this.props.item.price;
                var imageUrl = this.props.item.image.replace("%baseurl%", site.baseurl);
                var itemUrl = this.props.item.vei_url;
                var isVei = this.props.vei;
                var desc = this.props.singular ? this.props.item.desc : this.props.item.shortDesc;
                var renderFunc = this.props.singular ? this.renderSingular : this.renderList;
                return renderFunc(name, price, imageUrl, itemUrl, isVei, desc);
            }
        });
        exports.Products = React.createClass({
            getInitialState: function getLoadingState() {
                return {items: [], loading: true, callback: this.props.callback}
            },
            componentDidMount: function loadProductsAndDisplay() {
                var _this = this;
                fire.child("products").on("value", function onProductData(v) {
                    var iterated = [];
                    $.iterateObjectAlphabetically(v.val(), function onValue(key, val) {
                        val = $.extend({itemId: key}, val);
                        iterated.push(
                            val
                        );
                    });
                    var state = $.extend({}, _this.state);
                    if (_this.state.callback) {
                        _this.state.callback();
                        _this.state.callback = undefined;
                    }
                    state.items = iterated;
                    state.loading = false;
                    _this.setState(state);
                });
            },
            render: function renderMultipleProducts() {
                if (this.state.loading) {
                    return <div>You shouldn't be seeing this loading message.<br/>
                        Please report this to support.</div>;
                }
                var parts = [];
                for (var i = 0; i < this.state.items.length; i++) {
                    var item = this.state.items[i];
                    parts.push(<exports.Product item={item} key={i}/>);
                }
                return <table className="table">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name - Price</th>
                    </tr>
                    </thead>
                    {parts}
                </table>;
            }
        });
        return exports;
    });
