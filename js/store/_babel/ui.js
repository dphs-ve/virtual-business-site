define(["jquery", "jqueryui", "react", "reactbs", "store/babel/realdata", "babel/ui", "internal/firebase", "jqueryhashchange"],
    function defineUI($, jqui, React, ReactBS, products, ui, fb) {
        var exports = {};
        exports.renderProducts = function renderProducts() {
            var callback = function whenProductsLoaded(fastDisplay) {
                ui.stopLoading("#panel", function clearLoader(whenDone) {
                    whenDone();
                    $(".cssload-loader").remove();
                }, fastDisplay);
            };
            var firstDisplay = true;
            var isFirstDisplay = function isFirstDisplayThenReset() {
                if (firstDisplay) {
                    firstDisplay = false;
                    return true;
                }
                return false;
            };
            var showOneProduct = function showOneProduct(product, fromVei) {
                var firstDisplay = isFirstDisplay();
                ui.startLoading("#panel", function onReady(whenComplete) {
                    whenComplete();
                    React.render(<products.Product item={ product }
                                                   showStore={restoreStore} singular={true} vei={fromVei}/>,
                        document.getElementById("product-panel"),
                        function rendered() {
                            callback(!firstDisplay);
                        });
                }, !firstDisplay);
            };
            $(window).hashchange(function displayHashProduct() {
                var hashData = window.location.hash.substring(1).split("@");
                var product = hashData[0];
                var fromVei = hashData[1] === "vei";
                if (product === "thankyou") {
                    // not a product :3
                    return;
                }
                if (!product) {
                    restoreStore();
                    return;
                }
                fb.thisFB.child("products").child(product).once("value",
                    function showProduct(p) {
                        var val = p.val();
                        if (!val) {
                            restoreStore();
                            return;
                        }
                        showOneProduct($.extend({itemId: product}, val), fromVei);
                    });
            });
            var restoreStore = function showStoreFromProduct() {
                var firstDisplay = isFirstDisplay();
                ui.startLoading("#panel", function onReady(whenComplete) {
                    whenComplete();
                    $("#product-panel").empty();
                    displayStore(firstDisplay);
                }, !firstDisplay);
            };
            var displayStore = function showStore(firstDisplay) {
                React.render(
                    <div><div style={{textAlign: "center"}}><h4>Reflex Store/Catalog</h4></div>
                        <products.Products callback={callback.bind(this, !firstDisplay)}/>
                    </div>,
                    document.getElementById("product-panel")
                );
            };
            $("#panel").append("<div id='product-panel' class='rendered' " +
                "style='display: none'></div>");
            ui.startsAsLoader("#panel");
            $(window).hashchange();
        };
        return exports;
    });
