define(["firebase"],
    function (Firebase) {
        var exports = {};
        var newFirebase = exports.newFirebase = function firebaseCreateNewFirebase(base) {
            return new Firebase("https://" + base + ".firebaseio.com");
        };
        var thisFB = exports.thisFB = newFirebase("dphsve");
        exports.authenticateAdmin = function authenticateAdminByUserPass(user, pass, onFinish, onSuccess, onFail) {
            try {
                thisFB.authWithPassword({
                    email: user,
                    password: pass
                }, function (error, authData) {
                    onFinish();
                    if (error) {
                        onFail(error);
                    } else {
                        onSuccess(authData);
                    }
                });
            } catch (err) {
                onFinish();
                onFail(err);
            }
        };
        return exports;
    });
