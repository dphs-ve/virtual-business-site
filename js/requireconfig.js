requirejs.config({
    // libs are in lib, other stuff in app
    baseUrl: site.baseurl + "/js",
    paths: {
        internal: "./internal",
        libs: "./libs",
        jquery: [
            "//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min",
            "libs/jquery-2.1.4.min"
        ],
        jqueryui: [
            "//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min",
            "libs/jqueryui-1.11.4.min"
        ],
        jqueryhashchange: [
            //"//cdnjs.cloudflare.com/ajax/libs/jquery-hashchange/1.3/jquery.ba-hashchange.min",
            "libs/jqueryhashchange-1.3.min"
        ],
        jquerytransit: [
            "libs/jquerytransit-0.9.12.min"
        ],
        firebase: [
            "//cdn.firebase.com/js/client/2.3.1/firebase",
            "libs/firebase-2.3.1.min"
        ],
        react: [
            "//cdnjs.cloudflare.com/ajax/libs/react/0.13.3/react",
            "libs/react-0.13.3.min"
        ],
        reactbs: [
            "//cdnjs.cloudflare.com/ajax/libs/react-bootstrap/0.25.2/react-bootstrap.min",
            "libs/react-bootstrap-0.25.2.min"
        ],
        bs: [
            "//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min",
            "libs/bootstrap-3.3.3.min"
        ],
        "pnotify.custom": [
            "libs/pnotify.custom"
        ],
        "pnotify.custom.min": [
            "libs/pnotify.custom.min"
        ]
    },
    shim: {
        "bs": ["jquery"],
        "jqueryhashchange": ["jquery"],
        "jquerytransit": ["jquery"],
        "react": ["jquery"],
        "reactbs": ["react", "bs", "jquery"],
        "firebase": {
            exports: "Firebase"
        }
    }
});
define("noop", [], function defineNoopModule() {
    var exports = {
        noop: function noop() {
        }
    };
    window.noop = exports.noop;
    return exports;
});

require(["jquery"], function ($) {
    $.iterateObjectAlphabetically = function iterateObjectAlphabetically(obj, callback) {
        if (!obj) {
            return;
        }
        var keys = Object.keys(obj);
        keys.sort();
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            if (callback) {
                callback(key, obj[key], obj);
            }
        }
    };
});
