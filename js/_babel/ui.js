define(["jqueryui", "react", "reactbs", "noop", "pnotify", "pnotify.buttons", "pnotify.nonblock", "pnotify.callbacks", "jquerytransit"],
    function defineUI(jqui, React, rbs, noop, PNotify) {
        var exports = {};
        var devEnvironment = window.location.hostname !== "dphsrop.com";
        var displayStacktrace = devEnvironment ? function STACK_TRACE_DISPLAYER() {
            var e = new Error("dummy");
            var stack = e.stack.replace(/^[^\(]+?[\n$]/gm, "")
                .replace(/^\s+at\s+/gm, "")
                .replace(/^Object.<anonymous>\s*\(/gm, "{anonymous}()@")
                .split("\n");
            console.log(stack);
        } : noop.noop;
        // We use Bootstrap.
        PNotify.prototype.options.styling = "bootstrap3";
        // class RemovableAlert
        var RemovableAlert = function RemovableAlert(pnotify) {
            pnotify.modules["removableAlert"] = this;
            this.pnotify = pnotify;
            this.alertOpen = false;
            this.__openCb = undefined;
            var _this = this;
            this.afterOpen = function onAlertOpened() {
                if (_this.__openCb) {
                    _this.__openCb();
                }
                _this.alertOpen = true;
            };
            this.pnotify.open();
        };
        RemovableAlert.prototype.__addOpenCb = function addOpenCb(callback) {
            // Adds an open callback.
            if (this.alertOpen) {
                callback();
                return;
            }
            this.__openCb = callback;
        };
        RemovableAlert.prototype.remove = function remove(callback) {
            // Removes the alert when it is ready.
            var pnotify = this.pnotify;
            var _this = this;
            this.__addOpenCb(function remove() {
                // remove after timeout for lols
                setTimeout(function doAllRemove() {
                    _this.afterClose = function afterFullyRemoved() {
                        try {
                            if (callback) {
                                callback();
                            }
                        } finally {
                            _this.afterClose = undefined;
                        }
                    };
                    pnotify.remove();
                }, 1000);
            })
        };
        // endclass RemovableAlert
        // Put the stack on the left, It looks better.
        var stack = {"dir1": "down", "dir2": "right"};
        var pushAlert = exports.pushAlert = function pushAlertByPNotify(msg, type) {
            var usedType = type || "";
            return new RemovableAlert(new PNotify({
                animation: "slide",
                animate_speed: "medium",
                shadow: false,
                text: msg,
                type: usedType,
                icon: false,
                stack: stack,
                addclass: "stack-topleft",
                auto_display: false, // we'll handle it in RemovableAlert
                buttons: {
                    sticker: false
                }
            }));
        };
        var loadVisible = {};
        exports.startsAsLoader = function setLoadVisible(selector) {
            loadVisible[selector] = true;
        };
        var fastLikeTime = function convertTimeToFastTime(fastLike, time) {
            return fastLike ? time / 2 : time;
        };
        var processingStart = {};
        var processingStop = {};
        var doWhenStartComplete = {};
        var doWhenStopComplete = {};
        exports.startLoading = function startLoading(selector, onComplete, fastLike) {
            if (processingStart[selector] || doWhenStopComplete[selector]) {
                return;
            }
            if (processingStop[selector]) {
                doWhenStopComplete[selector] = function doStartLoading() {
                    exports.startLoading(selector, onComplete);
                };
                return;
            }
            processingStart[selector] = true;
            var allDone = function allDone() {
                processingStart[selector] = false;
                loadVisible[selector] = true;
                var func = doWhenStartComplete[selector];
                delete doWhenStartComplete[selector];
                if (func) {
                    func();
                }
            };
            displayStacktrace();
            var $sel = $(selector);
            var $parent = $($sel.parent()[0]);
            $sel.css("min-height", $parent.height());
            if (loadVisible[selector]) {
                if (onComplete) {
                    onComplete(noop.noop);
                }
                allDone();
                return;
            }
            $sel.children(".rendered").hide({
                effect: "blind", duration: fastLikeTime(fastLike, 1000), easing: "linear",
                complete: function afterHidden() {
                    var loadHtml = $("<div class=\"cssload-loader\" style='visibility: hidden;'>" +
                        "<div class=\"cssload-loader-spinny\">" +
                        "<div class=\"cssload-inner cssload-one\"></div>" +
                        "<div class=\"cssload-inner cssload-two\"></div>" +
                        "<div class=\"cssload-inner cssload-three\"></div>" +
                        "</div>" +
                        "</div>");
                    $sel.append(loadHtml);
                    var $loader = $($sel.children(".cssload-loader")[0]).children(".cssload-loader-spinny");
                    var targetHeight = $loader.height();
                    var targetWidth = $loader.width();
                    var targetLeft = $loader.position().left;
                    $sel.transition({
                        "min-height": targetHeight
                    }, fastLikeTime(fastLike, 500));
                    $loader.css({
                        height: 0,
                        width: 0,
                        top: $loader.height() / 2,
                        left: $loader.position().left + $loader.width() / 2
                    });
                    $loader.css("visibility", "visible");
                    var whenRemoved = function whenOnCompleteRemoves() {
                        $loader.transition({
                            height: targetHeight,
                            width: targetWidth,
                            top: 0,
                            left: targetLeft
                        }, fastLikeTime(fastLike, 1250), function onCompletelyRendered() {
                            if ($sel.children(".cssload-loader")[0]) {
                                $sel.transition({
                                    "min-height": 0
                                }, 100, allDone);
                            }
                        });
                    };
                    if (!onComplete) {
                        whenRemoved();
                    } else {
                        onComplete(whenRemoved);
                    }
                }
            });
        };
        exports.stopLoading = function stopLoading(selector, onComplete, fastLike) {
            if (processingStop[selector] || doWhenStartComplete[selector]) {
                return;
            }
            if (processingStart[selector]) {
                doWhenStartComplete[selector] = function doStopLoading() {
                    exports.stopLoading(selector, onComplete);
                };
                return;
            }
            processingStop[selector] = true;
            var allDone = function allDone() {
                processingStop[selector] = false;
                loadVisible[selector] = false;
                var func = doWhenStopComplete[selector];
                delete doWhenStopComplete[selector];
                if (func) {
                    func();
                }
            };
            displayStacktrace();
            var $sel = $(selector);
            var $loader = $($sel.children(".cssload-loader")[0]);
            if (!loadVisible[selector]) {
                if (onComplete) {
                    onComplete(noop.noop);
                }
                allDone();
                return;
            }
            var $parent = $($sel.parent()[0]);
            var finish = function removeLoadingAndShow() {
                $loader.children("p").slideUp();
                $loader.children(".cssload-loader-spinny").remove();
                // onComplete USUALLY adds some content to $sel....show it when done
                var whenRendered = function whenOnCompleteRenders() {
                    $sel.children(".rendered").show({
                        effect: "blind", duration: fastLikeTime(fastLike, 1000), easing: "linear",
                        complete: function onCompletelyRendered() {
                            // just in case there's actually no content
                            $sel.transition({
                                "min-height": 0
                            }, 100, allDone);
                        }
                    });
                };
                if (!onComplete) {
                    // no onComplete - just continue
                    whenRendered();
                } else {
                    onComplete(whenRendered);
                }
            };
            $sel.css("min-height", $parent.height());
            if (!$loader) {
                pushAlert("no loader", "info");
                finish();
                return;
            }
            $loader.children(".cssload-loader-spinny").transition({
                scale: "0, 0"
            }, fastLikeTime(fastLike, 1000), finish);
        };
        return exports;
    });
