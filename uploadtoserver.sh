#!/usr/bin/env bash
echo "Welcome to the automated FTP uploader."
username="$1"
pass="$2"
if [ "x$username" == "x" ]; then
    printf "Username: "
    read username
fi
if [ "x$pass" == "x" ]; then
    stty -echo
    printf "Password: "
    read pass
    stty echo
    printf "\n"
fi
echo "Uploading now..."
cd _site
# login and put _site
set -o verbose
lftp -u "$username","$pass" dphsrop.com << EOT
set ssl:verify-certificate false
mirror -Rev . .
EOT
set +o verbose
echo "Completed."
