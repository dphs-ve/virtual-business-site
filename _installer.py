#!/usr/bin/env python3
import _installer_cmd as cmdhelper
import sh
import sys

if len(sys.argv) == 1:
    print("Usage: " + sys.argv[0] + " <password>")
    sys.exit(1)
password = sys.argv[1] + '\n'

import os
debug = os.environ.get('INSTALLER_DEBUG', '0') == '1'


def command_exists(cmd):
    try:
        sh.Command(cmd)
        return True
    except sh.CommandNotFound:
        return False


def upgrade_sh(shcmd):
    """
    Upgrades a sh.py command to have TTY features, for reals.
    """
    return cmdhelper.bake_nice_tty(shcmd)

def upgrade_if_debug(shcmd):
    return upgrade_sh(shcmd) if debug else shcmd

# Insert password, no prompt.
sudo = sh.sudo.bake('-S', '-p', '', _in=password)
# All commands this script knows how to install its dependencies with.
possible_commands = ('brew', 'apt-get')
# ADD APT/BREW PACKAGES HERE #
packages = {
    'brew': ('nodejs', 'lftp'),
    'apt-get': ('nodejs', 'npm', 'lftp')
}
manager_cmds = {
    'brew': {
        'installcmd': lambda x: sh.brew.install(x),
        'upgradecmd': lambda x: sh.brew.upgrade(x, _ok_code=[0, 1]),
        'install_checker':
        lambda x: sh.brew.ls('--versions', x).stdout.strip() != ''
    },
    'apt-get': {
        'installcmd': lambda x: upgrade_sh(sudo.bake('apt-get', 'install', '-y'))(x),
        'upgradecmd': lambda x: upgrade_sh(sudo.bake('apt-get', 'upgrade', '-y'))(x),
        'install_checker':
        lambda x: sh.grep(sh.dpkg_query('-W', '--showformat=\'${Status}\n\'',
                                        x, _piped=True),
                          "install ok installed") != ''
    }
}


def do_install_all(usedpkgs, cmdset):
    installcmd = cmdset['installcmd']
    upgradecmd = cmdset['upgradecmd']
    install_checker = cmdset['install_checker']
    for package in usedpkgs:
            installed = install_checker(package)
            if installed:
                print(package + " already installed.")
                print("Updating " + package + " if possible...")
                upgradecmd(package)
                print("Updated " + package + ".")
            else:
                print("Installing " + package + "...")
                installcmd(package)
                print("Installed " + package + ".")
print("Checking for apt/brew packages {}...".format(packages))
anyinstall = False
for cmd in possible_commands:
    if command_exists(cmd):
        print("Installing with {}...".format(cmd))
        anyinstall = True
        do_install_all(packages[cmd], manager_cmds[cmd])
if not anyinstall:
    print("Did not find any support commands: " + str(possible_commands))
    sys.exit(1)

# get ruby
if not command_exists('rvm'):
    print('Installing rvm + ruby...')
    upgrade_sh(sh.bash)(sh.curl('-sSL', 'https://get.rvm.io',
                                _piped=True),
                        '-s', 'stable', '--ruby')
    print('Installed rvm + ruby.')
    print("Installing bundler via RubyGems...")
    upgrade_sh(sudo.gem.install)('bundler')
    print("Installed bundler.")

# if bundler isn't installed, install it (prevent re-running gem-install)
if command_exists('bundle'):
    print("Bundler installed.")
else:
    print("Critical error, no bundle command after installing bundler")
    sys.exit(1)

print("Using bundler to install dependencies...")
upgrade_sh(sh.bundle.install)('--path', 'vendor/install')
print("Bundle dependencies installed.")

# ADD NPM PACKAGES HERE #
npmpackages = ('babel-core', 'babel-cli', 'eslint',
               'babel-eslint', 'eslint-plugin-react')

print("Checking for npm packages {}...".format(npmpackages))
npmbase = sh.npm.bake('list', '-g')
if debug:
    upgrade_sh(npmbase)()
npmlist = npmbase().stdout.decode('utf8')
for package in npmpackages:
    if package in npmlist:
        print(package + " already installed.")
        print("Updating " + package + " if possible...")
        upgrade_sh(sudo.npm.update)('-g', package)
        print("Updated " + package + ".")
    else:
        print("Installing " + package + " via npm...")
        upgrade_sh(sudo.npm.install)('-g', package)
        print("Installed " + package + ".")
print("Checked all npm packages.")

# ADD NPM LOCAL PACKAGES HERE #
npmpackages = ('babel-preset-es2015', 'babel-preset-react')


print("Checking for npm local packages {}...".format(npmpackages))
npmbase = sh.npm.bake('list')
if debug:
    upgrade_sh(npmbase)()
npmlist = npmbase().stdout.decode('utf8')
for package in npmpackages:
    if package in npmlist:
        print(package + " already installed.")
        print("Updating " + package + " if possible...")
        upgrade_sh(sudo.npm.update)(package)
        print("Updated " + package + ".")
    else:
        print("Installing " + package + " via npm...")
        upgrade_sh(sudo.npm.install)(package)
        print("Installed " + package + ".")
print("Checked all npm local packages.")
print("Done installing/updating.")
