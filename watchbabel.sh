#!/bin/bash
source .sharedbabel.sh
len=${#inputbabel[@]}
pids=()
for i in $(seq 0 $((len-1))); do
    echo babel --watch "${inputbabel[$i]}" --out-dir "${outputbabel[$i]}"
    babel --watch "${inputbabel[$i]}" --out-dir "${outputbabel[$i]}"&
    pids+=("$!")
done
trap 'kill $(jobs -pr)' SIGINT SIGTERM EXIT
len=${#pids[@]}
for i in $(seq 0 $((len-1))); do
    pid="${pids[$i]}"
    echo "Waiting for $pid"
    wait "$pid"
done
