All files are located here.

Development always occurs in `feature/<feature-name>` branches or directly in `develop`.

Instructions
====
## Setup
### Automated
Run `./installdeps.sh` to install everything.

### Manual
Run everything in `installdeps.sh` manually. You will need:

  - On Ubuntu: `ruby`, `ruby-dev`, `nodejs`, `nodejs-legacy`, `npm`, and `lftp`
  - On OSX: `ruby`, `ruby-build`, `nodejs`, and `lftp` from Homebrew
  - `bundler`
  - everything in the `Gemfile.lock`
  - Babel from `npm`

## Updating
1. `git pull` for updates
2. Run `./installdeps.sh` again.

## Running
1. `./run.sh` will start a server on [`0.0.0.0:4000`](http://localhost:4000/virtualbusiness/)
2. `./watchbabel.sh` will watch for Babel changes

It is advised to have both of these running at the same time.

## Uploading to dphsrop.com
Run `./uploadtoserver.sh`, providing credentials when requested.